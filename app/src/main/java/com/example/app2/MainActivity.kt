package com.example.app2

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnGo.setOnClickListener {
            val lastName = PersonSername.text.toString()
            val name = PersonName.text.toString()
            val secondName = PersonSName.text.toString()
            val age = PersonAge.text.toString().toInt()
            val hobby = PersonHobby.text.toString()
            Intent(this, InfoActivity::class.java).also {
                it.putExtra("EXTRA_LASTNAME", lastName)
                it.putExtra("EXTRA_NAME", name)
                it.putExtra("EXTRA_SECONDNAME", secondName)
                it.putExtra("EXTRA_AGE", age)
                it.putExtra("EXTRA_HOBBY", hobby)
                startActivity(it)
            }
        }
    }
}