package com.example.app2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val lastName = intent.getStringExtra("EXTRA_LASTNAME")
        val name = intent.getStringExtra("EXTRA_NAME")
        val secondName = intent.getStringExtra("EXTRA_SECONDNAME")
        val age = intent.getIntExtra("EXTRA_AGE", 0)
        val hobby = intent.getStringExtra("EXTRA_HOBBY")
        val personString1 = "Ваша фамилия $lastName"
        val personString2 = "Ваше имя: $name"
        val personString3 = "Ваше отчество: $secondName"
        val personString4 = "Ваш возраст: $age"
        val personString5 = "Ваше хобби: $hobby"
        var personString6: String = ""

        when(age) {
            in 0..20 -> personString6 = "Вам от 0 до 20 лет. Ваc зовут $lastName $name $secondName" +
                    "Вы увлекаетесь $hobby"
            in 20..40 -> personString6 = "Вам от 20 до 40 лет. Ваc зовут $lastName $name $secondName" +
                    "Вы увлекаетесь $hobby"
            in 40..60 -> personString6 = "Вам от 40 до 60 лет. Ваc зовут $lastName $name $secondName" +
                    "Вы увлекаетесь $hobby"
            in 60..80 -> personString6 = "Вам от 60 до 80 лет. Ваc зовут $lastName $name $secondName" +
                    "Вы увлекаетесь $hobby"
            in 80..100 -> personString6 = "Вам от 80 до 100 лет. Ваc зовут $lastName $name $secondName" +
                    "Вы увлекаетесь $hobby"
        }

        lastName2.text = personString1
        Name2.text = personString2
        SecondName2.text = personString3
        Age2.text = personString4
        Hobby2.text = personString5
        PersonTV.text = personString6
    }
}